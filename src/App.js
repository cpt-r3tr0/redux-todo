import React from 'react';
import {Provider} from 'react-redux';
import './App.css';
import MaterialForm from './MaterialForm';
import store from './store'
import showResult from './showResults';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


function App() {
  return (
    <Provider store={store}>
      <Container maxWidth="sm">
      <AppBar position="static" color="primary">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            Todo
          </Typography>
        </Toolbar>
      </AppBar>
      <div className= "App">
        <MaterialForm  onSubmit={showResult} /> 
      </div>
      </Container>
    </Provider>
  );
}

export default App;
