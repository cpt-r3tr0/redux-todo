import React from 'react';
import { Field, reduxForm } from 'redux-form';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl'
import Button from '@material-ui/core/Button';

const validate = values => {
    const errors = {}
    const requiredFields = [
      'title',
      'discription',
    ]
    requiredFields.forEach(field => {
      if (!values[field]) {
        errors[field] = 'Required'
      }
    })
    return errors
  }

const renderTextField = (
        { input, label, meta: { touched, error }, ...custom },
        ) => (
        <TextField
        hintText={label}
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
        />
  );

const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
      return
    } else {
      return <FormHelperText>{touched && error}</FormHelperText>
    }
  };

const clickHelper= ()=>{

};
const renderSelectField = (
    { input, label, meta: { touched, error }, children, 
    ...custom } )=>(
        <FormControl error={touched && error}>
            <Select
            native
            {...input}
            {...custom}
            inputProps={{
                name: '',
                id: ''
            }}
            >
            {children}
            </Select>
            {renderFromHelper({ touched, error })}
        </FormControl>
);

const MaterialForm = props => {
    const { handleSubmit, pristine, reset, submitting, classes } = props;
    return (
        <form onSubmit= {handleSubmit}>
            <div>
                <Field
                name="title"
                component={renderTextField}
                label="Title"
                />
            </div>
            <div>
                <Field 
                name="discription" 
                component={renderTextField} 
                label="Discription" 
                />
            </div>
            <div>
                <Field
                classes={classes}
                name="status"
                component={renderSelectField}
                label="Status"
                >
                <option value="todo">ToDo</option>
                <option value={'ongoing'}>Ongoing</option>
                <option value={'stalled'}>Stalled</option>
                <option value={'done'}>Done</option>
                </Field>
            </div>
            <div>
                <Field 
                name="Due" 
                component={renderTextField} 
                label="DueBy" 
                type="Date"
                />
            </div>
            <div>
                <Button type="submit" variant="contained" color="primary"
                    disabled={pristine || submitting} onClick={clickHelper}>
                Submit
                </Button>
                <Button type="button" variant="contained" color="primary" disabled={pristine || submitting} onClick={reset}>
                    Clear Values
                </Button>
            </div>
        </form>
    );
}

export default reduxForm({
    form: 'MaterialForm',
    validate,

})(MaterialForm)